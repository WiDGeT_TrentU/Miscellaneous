#! /bin/bash -l
#SBATCH -A b2014050
#SBATCH -p core -n 1
#SBATCH -t 200:00:00

mkdir M5_50000.6
cd /scratch/$SLURM_JOB_ID
mkdir M5_50000.6
cd M5_50000.6
cp /home/aaronsha/private/msABC20120315/msABC ./

Rscript /home/aaronsha/private/Theta/test_model5_50000.r /home/aaronsha/private/Theta/M5_50000.6/


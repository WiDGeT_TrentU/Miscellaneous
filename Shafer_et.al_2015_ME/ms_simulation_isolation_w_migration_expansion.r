args <- commandArgs(trailingOnly = TRUE)
outfilename=args[1]
log10_theta_values=-2
theta=10^(log10_theta_values)
#theta=runif(nrepsABC,min=0.001, max=0.1)
msnreps=4854
threshold=4854
sample_size=60
#MAF=0.05*sample_size
nrepsABC=1000
n1_prior=runif(nrepsABC,min=0.1622, max=2.9594)
n2_prior=runif(nrepsABC,min=2.0117, max=9.9857)
old_n1_prior=runif(nrepsABC,min=0.0358, max=1.9967)
old_n2_prior=runif(nrepsABC,min=0.0464, max=1.9993)
m1_prior=runif(nrepsABC,min=0.0295,max=19.8648)
m2_prior=runif(nrepsABC,min=2.25730,max=19.9822)
n1_g_prior=(log(n1_prior/old_n1_prior))
n2_g_prior=(log(n2_prior/old_n2_prior))
Ts_prior=runif(nrepsABC,min=0.1006,max=1.9988)


for (irep in 1:msnreps) {
	
	
	sumstats_table=matrix(0,nrepsABC,14)
	
	for (irepABC in 1:nrepsABC) {

		S1_class=0
		final_table=NULL
		while (S1_class<threshold) {	

			system(paste("./msABC ",sample_size," ",msnreps," -t ",theta," -r 0 75 -I 2 20 40 -n 1 ",n1_prior[irepABC]," -n 2 ",n2_prior[irepABC]," -g 1 ",n1_g_prior[irepABC]," -g 2 ",n2_g_prior[irepABC]," -m 1 2 ",m1_prior[irepABC]," -m 2 1 ",m2_prior[irepABC]," -ej ",Ts_prior[irepABC]," 2 1 -en ",Ts_prior[irepABC]," 1 ",old_n1_prior[irepABC]," -en ",Ts_prior[irepABC]," 2 ",old_n2_prior[irepABC]," -eg ",Ts_prior[irepABC]," 1 0.0 -eg ",Ts_prior[irepABC]," 2 0.0 --missing wal.all.missing.list --verbose > ms.out",sep=""))
			table <- read.table("ms.out", header=T)
			indices_class1=which(table$s_fst!="-NaN"&table$s_fst!="NaN"&table$s_segs==1)	
			S1_class=S1_class+length(indices_class1)
			final_table=rbind(final_table,table[indices_class1,])		
		}
		summary_stats=c(colMeans(final_table[1:threshold,c(1,4,5,8,9,10,12,14,19,20,21,24,34,41),drop=F]))
		sumstats_table[irepABC,]=t(summary_stats)
		theta_value=rep(theta,threshold)
		write.table(sumstats_table,file=paste(outfilename,"out_running",sep=""),row.names=F,quote=F,col.names=names(summary_stats),sep="\t",append=F)		
	}
	write.table(sumstats_table,file="mu_modelX_Xloci",row.names=FALSE,quote=F,col.names=names(summary_stats),sep="\t")
quit()
}
n


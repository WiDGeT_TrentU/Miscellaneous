#! /bin/bash -l
#SBATCH -A b2010060
#SBATCH -p core -n 1
#SBATCH -t 167:00:00

mkdir bottle_LWR_S1_4_50000.1
cd bottle_LWR_S1_4_50000.1
cp /home/aaronsha/private/msABC20120315/msABC ./
cp /home/aaronsha/private/NO_MAF_S1_4/FIXED/msnsam ./
R CMD BATCH /home/aaronsha/private/Bottle_fixed/S1_4/bottle_LwR_S1_4.r50000


#How to estimate the expected distribution of segregating sites per GBS/RASseq loci of a given size

#Required parameters
fragment_length=Output_from_sequencer
pi_estimate=Output_from_selected_software or related species
sample_size=N

#example
fragment_length=100
pi_estimate=0.001
sample_size=86
chrom=2*sample_size

#Function
segsites<-function(chrom=2*sample_size,frag=fragment_length,pi=pi_estimate){
  seg<-sum(1/seq(1,chrom))*pi_estimate*fragment_length
  return(seg)
}

lambda=segsites(2*sample_size,fragment_length,pi)
null_exp=dpois(0:8, lambda)
null_exp
hist(null_exp)
plot(null_exp)
